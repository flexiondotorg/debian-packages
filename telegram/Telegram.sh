#!/usr/bin/env bash

if [ "${XDG_CURRENT_DESKTOP}" == "MATE" ]; then
    dbus-launch /opt/telegram/telegram -noupdate $@
else
    /opt/telegram/telegram -noupdate $@
fi
