caja-ideviceinfo (1.0.0-1) UNRELEASED; urgency=medium

  * Initial release.

 -- Martin Wimpress <code@flexion.org>  Thu, 23 Jun 2016 16:06:45 +0100

nautilus-ideviceinfo (0.1.0-0ubuntu13) xenial; urgency=medium

  * No change rebuild with the new libimobiledevice version

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 11 Nov 2015 10:48:16 +0100

nautilus-ideviceinfo (0.1.0-0ubuntu12) wily; urgency=medium

  * No change rebuild for the new libplist soname

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 23 Jul 2015 18:32:19 +0200

nautilus-ideviceinfo (0.1.0-0ubuntu11) utopic; urgency=medium

  * Fix dh-autoreconf call in debian/rules.

 -- Logan Rosen <logan@ubuntu.com>  Sun, 28 Sep 2014 13:17:08 -0400

nautilus-ideviceinfo (0.1.0-0ubuntu10) utopic; urgency=high

  * No change rebuild against plist2.

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Mon, 28 Apr 2014 02:25:00 +0100

nautilus-ideviceinfo (0.1.0-0ubuntu9) saucy; urgency=low

  * Update patch 01-gtk3-migration to fix prototype for
    rb_segmented_bar_expose to use a cairo_t * for Gtk+ 3.x rather than a
    GdkEvent *.

 -- Loïc Minier <loic.minier@ubuntu.com>  Mon, 02 Sep 2013 17:13:10 +0200

nautilus-ideviceinfo (0.1.0-0ubuntu8) saucy; urgency=low

  * Fix watch file to use a relative URI for downloads to match
    libimobiledevice.org.
  * Drop configure and Makefile.in from new_mbpi_build_fix.patch as this
    package uses dh_autoreconf.
  * Add patch libimobiledevice-1-1-5 to support new libimobiledevice 1.0 >=
    1.1.5 API; likely fixes crashes with latest libimobiledevice.

 -- Loïc Minier <loic.minier@ubuntu.com>  Mon, 02 Sep 2013 14:37:07 +0200

nautilus-ideviceinfo (0.1.0-0ubuntu7) saucy; urgency=low

  * No-change rebuild against latest libimobiledevice

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 08 Aug 2013 12:24:24 -0400

nautilus-ideviceinfo (0.1.0-0ubuntu6) quantal; urgency=low

  * debian/patches/new_mbpi_build_fix.patch:
    - fix build issue with current mobile-broadband-provider-info

 -- Sebastien Bacher <seb128@ubuntu.com>  Fri, 10 Aug 2012 12:33:02 +0200

nautilus-ideviceinfo (0.1.0-0ubuntu5) precise; urgency=low

  * No-change-rebuild for libnautilus-extension transition

 -- Andreas Moog <amoog@ubuntu.com>  Sat, 10 Dec 2011 15:26:27 +0100

nautilus-ideviceinfo (0.1.0-0ubuntu4) oneiric; urgency=low

  * debian/control:
   - Build-depends on libgtk-3-dev.
   - Bump build-depends on libnautilus-extension-dev (>= 3.0.0).
   - Build-depends on dh-autoreconf.
  * debian/rules:
   - Use --with-autoreconf, needed by 01-gtk3-migration.patch.
  * debian/patches:
   - 01-gtk3-migration.patch: Migrate to gtk3, to make it compatible with
     nautilus >= 3.0.0 (LP: #832842). 

 -- Julien Lavergne <gilir@ubuntu.com>  Sun, 11 Sep 2011 00:30:38 +0200

nautilus-ideviceinfo (0.1.0-0ubuntu3) natty; urgency=low

  * Rebuild against current libimobiledevice
  * Bump standards to 3.9.1. No other changes

 -- Bhavani Shankar <bhavi@ubuntu.com>  Sat, 15 Jan 2011 13:21:31 +0530

nautilus-ideviceinfo (0.1.0-0ubuntu2) maverick; urgency=low

  * debian/copyright:
   - Fix license notice for MIT/X11.

 -- Julien Lavergne <gilir@ubuntu.com>  Sun, 08 Aug 2010 19:35:15 +0200

nautilus-ideviceinfo (0.1.0-0ubuntu1) maverick; urgency=low

  * Initial release (LP: #592758)
   - Shows various information about the device
   - Segmented bar shows disk usage of device
   - Detect and open jailbroken

 -- Julien Lavergne <gilir@ubuntu.com>  Fri, 30 Jul 2010 21:15:06 +0200
