Source: vala-panel-appmenu
Section: x11
Priority: optional
Maintainer: Konstantin P. <ria.freelander@gmail.com>
Build-Depends: budgie-core-dev,
               cmake (>= 2.8.0),
               debhelper (>= 9),
               dpkg-dev (>= 1.16.1.1),
               libbamf3-dev,
               libgtk-3-dev (>= 3.12.0),
               libmate-panel-applet-dev (>= 1.16),
               libpeas-dev (>= 1.2.0),
               libwnck-3-dev (>= 3.4.0),
               libxfce4panel-2.0-dev,
               libxfconf-0-dev,
               valac (>= 0.24.0),
               xfce4-panel-dev (>= 4.11.2),
#vala-panel-dev               
Standards-Version: 3.9.8
Homepage: https://github.com/rilian-la-te/vala-panel-appmenu
Vcs-Git: https://github.com/rilian-la-te/vala-panel-appmenu.git
Vcs-Browser: https://github.com/rilian-la-te/vala-panel-appmenu

Package: budgie-appmenu-applet
Architecture: any
Depends: bamfdaemon,
         vala-panel-appmenu-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: appmenu-qt,
            appmenu-qt5,
            libdbusmenu-glib4,
            libdbusmenu-gtk3-4,
            libdbusmenu-gtk4,
            unity-gtk2-module,
            unity-gtk3-module,
Description: Application Menu plugin for budgie-panel
 This is an Application Menu (Global Menu) plugin. It is built using
 the Unity protocol and libraries and provides all features found in
 the Unity implementation.
 .
 This package provides the plugin for Budgie.

Package: xfce4-appmenu-plugin
Section: xfce
Architecture: any
Depends: bamfdaemon,
         vala-panel-appmenu-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: appmenu-qt,
            appmenu-qt5,
            libdbusmenu-glib4,
            libdbusmenu-gtk3-4,
            libdbusmenu-gtk4,
            unity-gtk2-module,
            unity-gtk3-module,
Description: Application Menu plugin for xfce4-panel
 This is an Application Menu (Global Menu) plugin. It is built using
 the Unity protocol and libraries and provides all features found in
 the Unity implementation.
 .
 This package provides the plugin for XFCE. 

Package: mate-applet-appmenu
Architecture: any
Depends: bamfdaemon,
         vala-panel-appmenu-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: appmenu-qt,
            appmenu-qt5,
            libdbusmenu-glib4,
            libdbusmenu-gtk3-4,
            libdbusmenu-gtk4,
            unity-gtk2-module,
            unity-gtk3-module,
Description: Application Menu plugin for mate-panel
 This is an Application Menu (Global Menu) plugin. It is built using
 the Unity protocol and libraries and provides all features found in
 the Unity implementation.
 .
 This package provides the plugin for MATE.

#Package: vala-panel-appmenu
#Architecture: any
#Depends: bamfdaemon,
#         vala-panel-appmenu-common (= ${source:Version}),
#         ${misc:Depends},
#         ${shlibs:Depends},
#Recommends: appmenu-qt,
#            appmenu-qt5,
#            libdbusmenu-glib4,
#            libdbusmenu-gtk3-4,
#            libdbusmenu-gtk4,
#            unity-gtk2-module,
#            unity-gtk3-module,
#Description: Application Menu plugin for vala-panel
# This is an Application Menu (Global Menu) plugin. It is built using
# the Unity protocol and libraries and provides all features found in
# the Unity implementation.

Package: vala-panel-appmenu-common
Architecture: all
Depends: ${misc:Depends},
Description: Common files for Application Menu plugins
 This is an Application Menu (Global Menu) plugin. It is built using
 the Unity protocol and libraries and provides all features found in
 the Unity implementation.
 .
 This package provides common files.
